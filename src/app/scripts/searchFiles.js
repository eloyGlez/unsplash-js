import {loadMoreImg}from './loadMore';
import{orderOption} from './orderOption';
let count = 0;
const searchFiles = (data, qualityOption) => {
  data.results.map( (photo) => {
       let result = '';
       switch (qualityOption) {
         case "raw":
            result = `<img id="img" src= "${photo.urls.raw}">`;
           $("#photoResult").append(result);
           break;
         case "full":
            result = `<img id="img" src= "${photo.urls.full}">`;
           $("#photoResult").append(result);
           break;
         case "regular":
            result = `<img id="img" src= "${photo.urls.regular}">`;
           $("#photoResult").append(result);
           break;
         case "small":
            result = `<img id="img" src= "${photo.urls.small}">`;
           $("#photoResult").append(result);
           break;
         case "thumb":
            result = `<img id="img" src= "${photo.urls.thumb}">`;
           $("#photoResult").append(result);
           break;
         default:
            result = `<img id="img" src= "${photo.urls.raw}">
            `;
            
           $("#photoResult").append(result);
           break;
       }
       count += 1;
  });
  loadMoreImg(count,data);
  orderOption(data);

};
export { searchFiles };
