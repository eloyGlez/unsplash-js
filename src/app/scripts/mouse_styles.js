let lazyElements = document.getElementById("loadMoreImg");
const mouseOver = () => {
  /* SEARCH BTN */
  document.getElementById("searchBtn").style.backgroundColor = "#f6cd61";
  document.getElementById("searchBtn").style.color = "white";
  document.getElementById("searchBtn").style["boxShadow"] = " 0 0 10px #f3be39";
  document.getElementById("searchBtn").style["textShadow"] =
    "5px 5px 10px rgba(36, 36, 36, 0.932)";

  document.getElementById("searchBtn").style.cursor = "pointer";

  /* LOAD MORE IMAG BTN */
  if (lazyElements != null) {
    document.getElementById("loadMoreImg").style.backgroundColor = "#f6cd61";
    document.getElementById("loadMoreImg").style.color = "white";
    document.getElementById("loadMoreImg").style["boxShadow"] =
      " 0 0 10px #f3be39";
    document.getElementById("loadMoreImg").style["textShadow"] =
      "5px 5px 10px rgba(36, 36, 36, 0.932)";
    document.getElementById("loadMoreImg").style.cursor = "pointer";
  }
  /* HOVER IMGS */
  
};
const mouseOut = () => {
  /* SEARCH BTN */
  document.getElementById("searchBtn").style.backgroundColor = "";
  document.getElementById("searchBtn").style["boxShadow"] = "";
  /* LOAD MORE IMAG BTN */
  if (lazyElements != null) {
    document.getElementById("loadMoreImg").style.backgroundColor = "";
    document.getElementById("loadMoreImg").style["boxShadow"] = "";
  }
  /* HOVER IMGS */
};
export { mouseOver, mouseOut };
