import {searchFiles} from './searchFiles';
import {CLIENT_ID} from '../../env'
const login = () => {
  let query = document.getElementById("searchInput").value;
  let qualityOption = document.getElementById('qualityOption').value;
  /* console.log(qualityOption); */
  let url =
    "https://api.unsplash.com/search/photos/?client_id=" +
    CLIENT_ID +
    "&query=" +
    query;
  if (query != "") {
    //REQUEST TO API
    fetch(url)
      .then(function (data) {
        return data.json();
      })
      .then(function (data) {
          setTimeout(() => {
            console.log(data);
          }, 3000);
        searchFiles(data,qualityOption);
      });
  }
  else{
      alert('Make sure that you fill with a valid input.');
      document.getElementById("searchInput").focus(); 
  }
};

export { login };
