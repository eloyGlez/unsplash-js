const loadMoreImg = (count,data) => {
  let loadContainer = `<div class="loadContainer">
<h1>${count} files were loaded... </h1>
</div>`;
$("#navHeader").append(loadContainer);
if(data != ''){
    document.getElementById('searchBtn').disabled=true;
    document.getElementById('searchBtn').style.backgroundColor='#a8a8a8';
    document.getElementById('searchBtn').style.cursor='not-allowed';
}
};

export { loadMoreImg };
